const std = @import("std");

pub const Backend = enum {
    no_backend,
    glfw_opengl3,
};

pub const Options = struct {
    backend: Backend,
    shared: bool = false,
    use_freetype: bool = false,
};

pub const Package = struct {
    options: Options,
    zgui: *std.Build.Module,
    zgui_options: *std.Build.Module,
    zgui_c_cpp: *std.Build.CompileStep,

    pub fn link(pkg: Package, exe: *std.Build.CompileStep) void {
        exe.linkLibrary(pkg.zgui_c_cpp);
        exe.addModule("imgui", pkg.zgui);
    }
};

pub fn build(b: *std.Build) void {
    const Args = struct {
        options: Options,
    };

    const args = Args{
        .options = Options{
            .backend = b.option(Backend, "backend", "The backend to use. Must be specified") orelse .no_backend,
            .shared = b.option(bool, "shared", "Whether or not to link as shared or static") orelse false,
            .use_freetype = b.option(bool, "use_freetype", "Whether or not to use freetype") orelse false,
        },
    };

    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const step = b.addOptions();
    step.addOption(Backend, "backend", args.options.backend);
    step.addOption(bool, "shared", args.options.shared);

    const zgui_options = step.createModule();

    const zgui = b.addModule("imgui", .{
        .source_file = .{ .path = thisDir() ++ "/src/main.zig" },
        .dependencies = &.{
            .{ .name = "zgui_options", .module = zgui_options },
        },
    });
    _ = zgui;

    const zgui_c_cpp = if (args.options.shared) blk: {
        const lib = b.addSharedLibrary(.{
            .name = "zgui",
            .target = target,
            .optimize = optimize,
        });

        b.installArtifact(lib);
        if (target.isWindows()) {
            lib.defineCMacro("IMGUI_API", "__declspec(dllexport)");
            lib.defineCMacro("IMPLOT_API", "__declspec(dllexport)");
            lib.defineCMacro("ZGUI_API", "__declspec(dllexport)");
        }

        break :blk lib;
    } else b.addStaticLibrary(.{
        .name = "zgui",
        .target = target,
        .optimize = optimize,
    });

    zgui_c_cpp.addIncludePath(.{ .path = thisDir() ++ "/libs" });
    zgui_c_cpp.addIncludePath(.{ .path = thisDir() ++ "/libs/imgui" });

    zgui_c_cpp.linkLibC();
    zgui_c_cpp.linkLibCpp();

    const cflags = &.{"-fno-sanitize=undefined"};

    zgui_c_cpp.addCSourceFiles(
        .{
            .files = &.{
                thisDir() ++ "/src/zgui.cpp",

                thisDir() ++ "/libs/imgui/imgui.cpp",
                thisDir() ++ "/libs/imgui/imgui_widgets.cpp",
                thisDir() ++ "/libs/imgui/imgui_tables.cpp",
                thisDir() ++ "/libs/imgui/imgui_draw.cpp",
                thisDir() ++ "/libs/imgui/imgui_demo.cpp",

                thisDir() ++ "/libs/imgui/implot_demo.cpp",
                thisDir() ++ "/libs/imgui/implot.cpp",
                thisDir() ++ "/libs/imgui/implot_items.cpp",
                thisDir() ++ "/src/zgui.cpp",
            },
            .flags = cflags,
        },
    );

    if (args.options.use_freetype) {
        zgui_c_cpp.linkSystemLibrary("freetype2");
        zgui_c_cpp.defineCMacro("IMGUI_ENABLE_FREETYPE", null);

        zgui_c_cpp.addCSourceFile(.{ .file = .{ .path = thisDir() ++ "/libs/imgui/misc/freetype/imgui_freetype.cpp" }, .flags = cflags });
    }

    switch (args.options.backend) {
        .glfw_opengl3 => {
            zgui_c_cpp.linkSystemLibrary("opengl");
            zgui_c_cpp.addCSourceFiles(.{
                .files = &.{
                    thisDir() ++ "/libs/imgui/backends/imgui_impl_glfw.cpp",
                    thisDir() ++ "/libs/imgui/backends/imgui_impl_opengl3.cpp",
                },
                .flags = cflags,
            });
        },
        .no_backend => {},
    }

    b.installArtifact(zgui_c_cpp);
}

inline fn thisDir() []const u8 {
    return comptime std.fs.path.dirname(@src().file) orelse ".";
}
